# My Dotfiles

## Sync

```bash
dotsync
```

Or

```bash
cd ~/.files && make sync
```

## Manual

OS & Desktop: KDE Plasma + KWin + Wayland

Theme: Aritim-Dark (layout) + ChromeOS-Dark

Window Decorators: One-Dark

Keyboard: Delay: 300ms, Repeat: 50 r/s
